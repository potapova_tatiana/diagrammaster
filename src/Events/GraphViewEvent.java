/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Events;

import java.util.EventObject;

/**
 * Событие графа - представления
 * @author Tatiana
 */
public class GraphViewEvent extends EventObject{

    private boolean isClone;    //событие - клонирование
    private double dx;          //разница в перемещении по х
    private double dy;          //разница в перемещении по у
    private Object[] movingCells;   //перемещаемые объекты
    
    //конструктор
    public GraphViewEvent(Object source) {
        super(source);
    }
    
    //-------------------------------------сеттеры и геттеры-------------------------
    public double getDx(){
        return dx;
    }
    
    public void setDx(double dx){
        this.dx = dx;
    }
    
    public double getDy(){
        return dy;
    }
    
    public void setDy(double dy){
        this.dy = dy;
    }
    
    public void setMovingCells(Object[] cells){
        this.movingCells = cells;        
    }
    
    public Object[] getMovingCells(){
        return movingCells;        
    }
    
    public boolean getIsClone(){
        return isClone;
    }
    
    public void setIsClone(boolean flag){
        isClone = flag;
    }
}
