package Events;

/**
 * интерфейс слушателя событий графа - представления
 * @author Tatiana
 */
public interface GraphViewListener  {
    public void fireCellMoved(GraphViewEvent event);
}
