package Events;

/**
 * интерфейс слушателя событий модели графа
 * @author Tatiana
 */
public interface MyGraphModelListener {
    public void fireMessage(String message, String type);
}
