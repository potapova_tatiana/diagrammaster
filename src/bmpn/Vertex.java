
package bmpn;

import com.mxgraph.model.mxCell;

/**
 * Элемент клетка
 * @author Tatiana
 */
public class Vertex extends Element{

    public Vertex(String type) {
        super(type);
        ((mxCell)view).setVertex(true);
        
        int height = 40;
        int width = 40;
        String text = "";
        if (type.equalsIgnoreCase("task")) {
            text = "Задание";
            width = 80;
        } else if (type.equalsIgnoreCase("pool")) {
            text = "Пул";
            width = 100;
        }        
        saveNewValue(text);
        view.getGeometry().setWidth(width);
        view.getGeometry().setHeight(height);
    }    
}
