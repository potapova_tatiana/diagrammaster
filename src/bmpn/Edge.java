package bmpn;

/**
 * Элемент стрелка
 * @author Tatiana
 */
public class Edge extends Element{

    public Edge(String type) {  
        super(type);
        view.setEdge(true);        
    }    
}
