/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmpn;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;

/**
 * Элемент диаграммы
 * @author Tatiana
 */
public class Element {
    
    protected mxCell view;  //представление  
    protected String type;  //тип
    protected String value; //текстовое значение
    
    //конструктор
    Element(String type){  
        mxCell cell = new mxCell();
        cell.setGeometry(new mxGeometry());
        cell.setStyle(type);
        this.value = "";
        this.type = type;
        this.view = cell;
    }    
    
    //--------------------------------сеттеры и геттеры-------------------------
    public void setView(mxCell view){
        this.view = view;
        this.value = (String) view.getValue();
    }
    
    public mxCell getView(){
        return view;
    }
    
    public String getType(){
        return type;
    }

    //----------------------------- общедоступные функции ----------------------
    
    //показать полный текст элемента
    public void showRealText() {
        view.setValue(value);
    }

    //показать сокращенный текст элемента
    public void showValidatedText() {        
        String visibleText = value;
        if (value.length() >= 13){
            visibleText = value.substring(0, 12);
            visibleText += "...";
        }
        view.setValue(visibleText);
    }

    //установить новое значение элемента
    public void saveNewValue(String value) {
        this.value = value;
        showValidatedText();
    }
}
