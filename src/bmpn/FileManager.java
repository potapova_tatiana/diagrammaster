
package bmpn;


import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.ParentReference;
import java.util.Arrays;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxCellRenderer;

import com.mxgraph.util.png.mxPngEncodeParam;
import com.mxgraph.util.png.mxPngImageEncoder;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Менеджер по работе с файлами
 * @author trungchi321
 */
public class FileManager {

    private MainFrame parent;                   //родительское окно
    private java.io.File currentFile;           //текущий открытый файл
    private ArrayList<String> lastOpenedFiles;  //список последних открытых файлов

    // Данные для авторизации сервиса гугла
    private static final String SERVICE_ACCOUNT_EMAIL = "account-1@plucky-agent-115112.iam.gserviceaccount.com";
    private static String SERVICE_ACCOUNT_PKCS12_FILE_PATH;

    private String gmail = ""; // адрес пользователя
    private String url = "";   // ссылка на файл ( на сервере )

    private String property = System.getProperty("user.dir");   //текущая директория
    private String config;                                      //конфигурационный файл для сервиса гугл

    // Конструктор
    public FileManager(MainFrame parent) {
        this.parent = parent;
        currentFile = null;
        lastOpenedFiles = new ArrayList<>();
        updateLanguage();
        initLastOpenedFiles();
        config = property + "\\config\\key.p12";
        SERVICE_ACCOUNT_PKCS12_FILE_PATH = config;
        updateGmail();
    }

    //------------------------------------сеттеры и геттеры---------------------
    
    // Задает почту пользователя
    public void setGmail(String mail) {
        this.gmail = mail;    
        addGmailToConfig(mail);
    }
    
    public String getEmail(){
        return gmail;
    }

    // Возвращает адрес сохраненного на сервере файла
    public String getUrl() {
        return url;
    }

    // Возвращает список последних 5 открытых файлов
    public ArrayList<String> getLastFiveOpenedFiles() {
        ArrayList<String> result = new ArrayList();
        int size = lastOpenedFiles.size();
        for (int i = size - 1; i >= 0; i--) {
            String current = lastOpenedFiles.get(i);
            if (!result.contains(current)) {
                result.add(current);
            }
            if (result.size() > 4) {
                break;
            }
        }
        return result;
    }
    
    // Устанавливает текущий открытый файл
    public void setCurrentFile(java.io.File file) {
        currentFile = file;
    }

    // Возвращает текущий открытый файл
    public java.io.File getCurrentFile() {
        return currentFile;
    }
    
    //-------------------------------базовая работа с файлами-------------------
    
    // Загружает граф из файла
    public boolean loadGraphFromFile(GraphModel model, String filename) throws IOException {
        java.io.File file = null;
        //выбираем файл по его названию
        if (filename == null) {
            JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
            fileChooser.updateUI();
            fileChooser.setDialogTitle("Открыть файл");
            fileChooser.setFileFilter(new FileNameExtensionFilter("bpmndia", "bpmndia"));
            int rVal = fileChooser.showOpenDialog(parent);
            if (rVal == JFileChooser.APPROVE_OPTION) {
                file = fileChooser.getSelectedFile();
            }       
            else{
                return false;
            }
        } else {
            file = new java.io.File(filename);
        }
        //находим файл в системе
        BufferedReader bfr = null;
        try {
            bfr = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(parent,
                    "Не удалось найти файл по указанному расположению",
                    "Файл не найден!",
                    JOptionPane.WARNING_MESSAGE);
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //считываем контрольную сумму
        ArrayList<String> matrix = new ArrayList<>();
        String sCurrentLine;
        String codeMD5 = null;
        try {
            codeMD5 = bfr.readLine();
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String checkStr = "";

        try {
            while ((sCurrentLine = bfr.readLine()) != null) {
                matrix.add(sCurrentLine);
                checkStr += sCurrentLine + "\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        //считываем остальную часть файла
        if (codeMD5.equals(encryptMD5(checkStr))) {
            model.clearAll();
            model.paintGraphFromFile(matrix);
            model.setWasChanged(false);
            currentFile = file;
            addOpenedFileToLog(file.getPath());
            return true;
        } else {
            JOptionPane.showMessageDialog(parent,
                    "Файл поврежден!",
                    "Не удалось прочитать содержимое файла!",
                    JOptionPane.WARNING_MESSAGE);
        }
        return false;
    }

    // Сохраняет граф в текущий файл
    public void saveGraphToCurrentFile(GraphModel model) {        
        if (currentFile == null) {
            //текущего файла нет
            saveAsGraphToFile(model);
            model.setWasChanged(false);
        } else {
            //текущий файл есть
            try {
                try ( 
                    //записываем граф в файл
                    FileWriter fw = new FileWriter(new java.io.File(currentFile.getPath() + ".bpmndia"))) {
                    String matrix = model.getMatrixForSave();
                    String textMatrix = encryptMD5(matrix) + "\n" + matrix;
                    fw.write(textMatrix);
                    fw.flush();

                    try {
                        //отправляем на сервер
                        if (addFileInGoogleDrive(currentFile)) {
                            System.out.printf(url);
                        }
                    } catch (GeneralSecurityException ex) {
                        JOptionPane.showMessageDialog(parent,
                                "Ваша диаграмма не сохранена на сервере, попробуйте позже!",
                                "Произошла ошибка!",
                                JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (URISyntaxException ex) {
                        JOptionPane.showMessageDialog(parent,
                                "Ваша диаграмма не сохранена на сервере, попробуйте позже!",
                                "Произошла ошибка!",
                                JOptionPane.WARNING_MESSAGE);
                        Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                //записываем, что изменений нет
                model.setWasChanged(false);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(parent,
                        "Ваша диаграмма не сохранена на сервере, попробуйте позже!",
                        "Произошла ошибка!",
                        JOptionPane.WARNING_MESSAGE);
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                
            }
        }
    }

    // Сохраняет в файл с запросом файла 
    public void saveAsGraphToFile(GraphModel model) {
        //узнать файл для сохранения
        JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
        fileChooser.updateUI();
        fileChooser.setDialogTitle("Сохранить как");
        fileChooser.setFileFilter(new FileNameExtensionFilter("bpmndia", "bpmndia"));
        int rVal = fileChooser.showSaveDialog(parent);
        if (rVal == JFileChooser.APPROVE_OPTION) {

            //исправить название файла, если это пересохранение существующего 
            java.io.File file = fileChooser.getSelectedFile();
            String writingFilename = file.getPath();
            String extension = "";
            int i = writingFilename.lastIndexOf('.');
            int p = Math.max(writingFilename.lastIndexOf('/'), writingFilename.lastIndexOf('\\'));
            if (i > p) {
                extension = writingFilename.substring(i + 1);
            }
            if ("".equals(extension)) {
                writingFilename += ".bpmndia";
            }
            
            //создать файл для записи
            file = new java.io.File(writingFilename);
            try {
                try ( 
                    //записать файл
                    FileWriter fw = new FileWriter(file)) {
                    String matrix = model.getMatrixForSave();

                    String textMatrix = encryptMD5(matrix) + "\n" + matrix;
                    fw.write(textMatrix);

                    fw.flush();
                }
                //отметить, что изменений нет
                model.setWasChanged(false);
                //отметить текущий открытый файл 
                currentFile = file;
                //добавить открытый файл к логам
                addOpenedFileToLog(writingFilename);
                try {
                    //добавить файл на сервер
                    if (addFileInGoogleDrive(currentFile)) {
                        System.out.printf(url);
                    }
                } catch (GeneralSecurityException ex) {
                    JOptionPane.showMessageDialog(parent,
                            "Ваша диаграмма не сохранена на сервере, попробуйте позже!",
                            "Произошла ошибка!",
                            JOptionPane.WARNING_MESSAGE);
                    Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (URISyntaxException ex) {
                    JOptionPane.showMessageDialog(parent,
                            "Ваша диаграмма не сохранена на сервере, попробуйте позже!",
                            "Произошла ошибка!",
                            JOptionPane.WARNING_MESSAGE);
                    Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(parent,
                        "Ваша диаграмма не сохранена на сервере, попробуйте позже!",
                        "Произошла ошибка!",
                        JOptionPane.WARNING_MESSAGE);
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // Экспортирует в картинку
        public void exportImage(mxGraphComponent graphComponent, boolean needWatermarking, int rows, int cols) {

        //получить картинку графа
        BufferedImage image = mxCellRenderer.createBufferedImage(graphComponent.getGraph(),
                null, 1, Color.WHITE, graphComponent.isAntiAlias(), null, graphComponent.getCanvas());
        
        //граф пуст, не сохранять
        if (image == null) {
            JOptionPane.showMessageDialog(parent,
                    "В диаграмме нет элементов!",
                    "Сохранение не произойдет!",
                    JOptionPane.WARNING_MESSAGE);
            return;
        }

        //запросить файл для сохранения
        mxPngEncodeParam param = mxPngEncodeParam.getDefaultEncodeParam(image);
        String erXmlString = ".png";
        param.setCompressedText(new String[]{"mxGraphModel", erXmlString});

        JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
        fileChooser.updateUI();
        fileChooser.setDialogTitle("Экспорт в картинку");
        fileChooser.setFileFilter(new FileNameExtensionFilter(".png", "image file"));
        int rVal = fileChooser.showSaveDialog(parent);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            
            //открыть выбранный файл
            java.io.File chosenFile = fileChooser.getSelectedFile();

            FileOutputStream outputStream;

            try {
                //добавить водяной знак, если необходимо
                if (needWatermarking){
                    image = addWatermark(image, rows, cols);
                }
                //сохранить картинку в файл
                java.io.File file = new java.io.File(chosenFile.getPath() + ".png");
                outputStream = new FileOutputStream(file);
                mxPngImageEncoder encoder = new mxPngImageEncoder(outputStream, param);
                if (image != null) {
                    encoder.encode(image);
                }                
                outputStream.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
	

    //------------------------вспомогательные функции для работы----------------
    
    // Возвращает хэш-сумму строки
    private static String encryptMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // Устанавливает русский язык у диалогов
    private void updateLanguage() {
        UIManager.put("FileChooser.fileNameLabelText", "Имя файла:");
        UIManager.put("FileChooser.lookInLabelText", "Папка:");
        UIManager.put("FileChooser.cancelButtonText", "Отмена");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Тип файла:");
        UIManager.put("FileChooser.saveButtonText", "Сохранить");
        UIManager.put("FileChooser.openButtonText", "Открыть");
        UIManager.put("FileChooser.acceptAllFileFilterText", "Все файлы");
    }

    // Добавляет водяной знак на файл-картинку
    // Добавляет водяной знак на файл-картинку
    private BufferedImage addWatermark(BufferedImage image, int rows, int cols) throws FileNotFoundException, IOException {
       
        //считываем картинку как исходную и как накладываемый водяной знак 
        BufferedImage sourceImage = image;
        BufferedImage watermark = image;

        //определяем высоту и ширину одного водяного знака
        int width = sourceImage.getWidth() / cols;
        int height = sourceImage.getHeight() / rows;

        //подготавливаем водяной знак для наложения
        Image tmp = watermark.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage watermarkImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d2 = watermarkImage.createGraphics();
        g2d2.drawImage(tmp, 0, 0, null);
        g2d2.dispose();

        // подготавливаем картинку для наложения ВЗ
        Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
        AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);
        g2d.setComposite(alphaChannel);

        //накладываем водяные знаки
        for (int topLeftY = 0; topLeftY < sourceImage.getHeight(); topLeftY += height) {
            for (int topLeftX = 0; topLeftX < sourceImage.getWidth(); topLeftX += width) {
                // paints the image watermark
                g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);
            }
        }

        g2d.dispose();
        return sourceImage;
    }
    // Добавляет имя файла в лог
    private void addOpenedFileToLog(String addingFileName) {
        try {
            config = property + "\\config\\filelist.dat";
            FileWriter sw = new FileWriter(new java.io.File (config), true);
            sw.write(addingFileName + "\n");
            sw.close();
            lastOpenedFiles.add(addingFileName);
            parent.updateLastOpenedFiles();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // Добавляет gmailа в файл
    private boolean addGmailToConfig(String gmail) {
        try {
            config = property + "\\config\\config.edit";
            FileWriter sw = new FileWriter(new java.io.File (config), false);
            sw.write(gmail);
            sw.close();
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return false;
        }
        return true;
    }

    // Проверяет наличия Gmailа в настройках - сохранение или чтение оного
    private void updateGmail() {
        java.io.File configFile = new java.io.File(property + "\\config\\config.edit");
        if (configFile.exists()) {
            try {
                BufferedReader bfr = new BufferedReader(new FileReader(configFile));
                String sCurrentLine;
                try {
                    while ((sCurrentLine = bfr.readLine()) != null) {
                        gmail = sCurrentLine;
                    }
                } catch (IOException ex) {
                    System.out.print(ex.getMessage());
                }
            } catch (FileNotFoundException ex) {
                System.out.print(ex.getMessage());
            }
        }
    }

    // Инициализирует список последних открытых файлов
    private void initLastOpenedFiles() {
        lastOpenedFiles.clear();
        config = property + "\\config\\filelist.dat";
        java.io.File configFile = new java.io.File(property + "\\config\\filelist.dat");
        if (configFile.exists()){
            try {
                BufferedReader bfr = new BufferedReader(new FileReader(configFile));
                String sCurrentLine;
                try {
                    while ((sCurrentLine = bfr.readLine()) != null) {
                        lastOpenedFiles.add(sCurrentLine);
                    }
                } catch (IOException ex) {
                    System.out.print(ex.getMessage());
                }
            } catch (FileNotFoundException ex) {
                System.out.print(ex.getMessage());
            }
        }
    }

    // Добавляет файл на сервер, возвращает успех операции
    private boolean addFileInGoogleDrive(java.io.File currentFile) throws GeneralSecurityException, IOException, URISyntaxException {

        // Если не настроен gmail - не отправляем на сервер
        if ("".equals(this.gmail)) {
            return false;
        }
        // Сообщение
        Object[] options = {"Да", "Нет"};
        int n = JOptionPane.showOptionDialog(parent,
                "Вы можете отправить файл на сервер. Это займет некоторое время. Хотите сделать это?", "Подтверждение",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        if (n == 1) {
            return false;
        }
        // Получаем сервис для работы
        Drive client = getDriveService();
        // Отправляем файл
        File file = insertFile(client, currentFile.getName(), "", "", "*/*", currentFile.getName());
        // Добавляем разрешения для пользователя
        shareFileOrFolder(client, file);
        // Возврат ссылки нахождения файла
        url = file.getAlternateLink();
        // Открытие браузера с ссылкой на файл
        Desktop.getDesktop().browse(new URI(url));
        // Сообщение
        JOptionPane.showMessageDialog(parent,
                "Ваша диаграмма сохранена на сервере. Сейчас откроется ваш браузер с ссылкой на файл.",
                "Сохранение прошло успешно!",
                JOptionPane.INFORMATION_MESSAGE);
        return true;
    }

    // Получение сервиса гугла
    private Drive getDriveService() throws GeneralSecurityException, IOException, URISyntaxException {
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jsonFactory = new JacksonFactory();
        List<String> scopes = new ArrayList<String>();
        scopes.add(DriveScopes.DRIVE);
        GoogleCredential credential;
        credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
                .setServiceAccountScopes(scopes)
                .setServiceAccountPrivateKeyFromP12File(new java.io.File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
                .build();
        Drive service = new Drive.Builder(httpTransport, jsonFactory, null)
                .setHttpRequestInitializer(credential).build();
        return service;
    }

    // Отправка файла на сервер
    private File insertFile(Drive service, String title, String description,
            String parentId, String mimeType, String filename) throws IOException {

        File body = new File();
        body.setTitle(title);
        body.setDescription(description);
        body.setMimeType(mimeType);

        if (parentId != null && parentId.length() > 0) {
            body.setParents(
                    Arrays.asList(new ParentReference().setId(parentId)));
        }

        java.io.File fileContent = new java.io.File(filename);
        FileContent mediaContent = new FileContent(mimeType, fileContent);

        File file = service.files().insert(body, mediaContent).execute();
        return file;
    }

    // Выдача прав пользователю для работы с файлом
    private void shareFileOrFolder(Drive service, File file)
            throws IOException {

        Permission newPermission = new Permission();
        newPermission.setEmailAddress(gmail);
        newPermission.setValue(gmail);
        newPermission.setType("user");
        newPermission.setRole("writer");
        service.permissions().insert(file.getId(), newPermission).execute();
    }

}
