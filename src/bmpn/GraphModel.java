/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmpn;

import Events.GraphViewEvent;
import Events.GraphViewListener;
import Events.MyGraphModelListener;
import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxRectangle;
import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Модель графа
 *
 * @author Tatiana
 */
public class GraphModel {

    private boolean wasChanged;                             //были ли изменения
    private final GraphView graphView = new GraphView();    //представление
    private Element creatingElement;                        //создаваемый элемент
    private String currentAction = "";                      //текущее действие
    private final ArrayList<Element> connectingElements = new ArrayList<>(); //соединяемые элементы
    private ButtonsController controller;                   //контроллер кнопок
    private ArrayList<Element> elements = new ArrayList();  //существующие элементы
    private mxGraphComponent graphComponent;                //компонент графа
    private MyGraphModelListener myGraphModelListener;      //слушатель модели графа

    // Конструктор
    public GraphModel() {
        wasChanged = false;
        graphView.setGraphViewListener(new GraphViewObserver());
    }

    // -------------------------------сеттеры и геттеры-------------------------
    // Задает компонент графа
    public void setGraphComponent(mxGraphComponent graphComp) {
        graphComponent = graphComp;
        graphComponent.addListener(mxEvent.LABEL_CHANGED, new TextEditingListener());
        graphComponent.addListener(mxEvent.START_EDITING, new TextStartEditingListener());
    }

    // Задает слушателя модели графа
    public void setMyGraphModelListener(MyGraphModelListener listener) {
        myGraphModelListener = listener;
    }

    // Возвращает слушателя модели
    public MyGraphModelListener getMyGraphModelListener() {
        return myGraphModelListener;
    }

    // Возвращает, были ли изменения    
    public boolean getWasChanged() {
        return wasChanged;
    }

    // Устанавливает, были ли изменения
    public void setWasChanged(boolean wasChanged) {
        this.wasChanged = wasChanged;
    }

    // Задает контроллера кнопок
    public void setController(ButtonsController controller) {
        this.controller = controller;
    }

    // Возвращает представление графа
    public GraphView getGraphView() {
        return graphView;
    }

    // Задает тип создаваемого элемента
    public void setCreatingElementType(String elementType) {
        if ("dashArrow".equals(elementType) || "arrow".equals(elementType)) {
            creatingElement = new Edge(elementType);
        } else {
            creatingElement = new Vertex(elementType);
        }
    }

    // Задает текущее действие
    public void setAction(String action) {
        connectingElements.clear();
        currentAction = action;
    }

    // Возвращает список элементов
    public ArrayList<Element> getElements() {
        return elements;
    }

    //---------------------------действия над элементами------------------------
    // Добавляет созданный элемент по клику
    public void mousePressed(int x, int y) throws IOException {
        if (currentAction.equalsIgnoreCase("create element")) {
            if (creatingElement.getType() == "databased") {
                Templates template = new Templates(this);
                if (!template.addExclusiveGateway(x, y)) {
                    return;
                }
                template = null;
                currentAction = "";
                controller.actionDone();
            } else {
                Object view = graphView.addMyCell(creatingElement, x, y);
                boolean collision = checkCollisionsForCell((mxCell) view);
                if (collision) {
                    Object[] cells = new Object[1];
                    cells[0] = view;
                    graphView.removeCells(cells);
                    myGraphModelListener.fireMessage("Нельзя добавить элемент в это место", "Ошибка");
                } else {
                    elements.add(creatingElement);
                    wasChanged = true;
                    currentAction = "";
                    controller.actionDone();
                    System.out.println("create element clicked!");
                    repaintAllArrows();
                }
            }
        } else if (currentAction.equalsIgnoreCase("create edge")) {
            Object objectView = graphView.getElementByXY(x, y);
            if (objectView != null) {
                connectingElements.add(getElementByView(objectView));
            }
            if (connectingElements.size() > 1) {
                Element first = connectingElements.get(0);
                Element second = connectingElements.get(1);
                graphView.addMyEdge(creatingElement, first, second);
                elements.add(creatingElement);
                connectingElements.clear();
                wasChanged = true;
                currentAction = "";
                controller.actionDone();
            }
        } else if (currentAction.equalsIgnoreCase("delete")) {
            deleteElements();
            currentAction = "";
            wasChanged = true;
            controller.actionDone();
            System.out.println("delete clicked!");
        }
    }

    // Удаляет выбранные элементы
    public void deleteElements() {
        Object[] objs = graphView.getSelectionCells();
        graphView.getModel().beginUpdate();
        try {
            if (objs != null) {
                for (Object obj : objs) {
                    Element element = getElementByView(obj);
                    if (element != null) {
                        elements.remove(element);
                        wasChanged = true;
                    }
                    mxCell cell = (mxCell) obj;
                    for (int i = 0; i < cell.getEdgeCount();) {
                        Object o = cell.getEdgeAt(i);
                        Element tmpElement = getElementByView(o);
                        elements.remove(tmpElement);
                        graphView.getModel().remove(o);
                        wasChanged = true;
                    }
                    graphView.getModel().remove(cell);
                }
            }
        } finally {
            graphView.getModel().endUpdate();
        }
    }

    // Очищает граф
    public void clearAll() {
        graphView.selectAll();
        deleteElements();
        graphView.setSelectionCell(null);
    }

    //---------------------------действия, связанные с работой с файлами--------
    // Возвращает граф в виде строки
    public String getMatrixForSave() {

        ArrayList<Vertex> vertexes = new ArrayList<>();
        ArrayList<Edge> edges = new ArrayList<>();

        for (Element element : elements) {
            element.showRealText();
            if (element instanceof Vertex) {
                vertexes.add((Vertex) element);
            } else if (element instanceof Edge) {
                edges.add((Edge) element);
            }
        }

        String matrix = vertexes.size() + "\n";

        for (Vertex element : vertexes) {
            String value = "";
            mxCell cell = (mxCell) element.getView();
            value += element.getType() + "#" + cell.getId() + "#" + cell.getValue()
                    + "#" + cell.getGeometry().getX() + "#" + cell.getGeometry().getY() + "\n";

            matrix += value;
        }

        matrix += edges.size() + "\n";

        for (Edge element : edges) {
            String value = "";
            mxCell edge = (mxCell) element.getView();;
            value += element.getType() + "#" + edge.getId() + "#" + edge.getValue()
                    + "#" + edge.getSource().getId() + "#" + edge.getTarget().getId() + "\n";

            matrix += value;
        }

        for (Element element : elements) {
            element.showValidatedText();
        }

        return matrix;
    }

    // Создает граф по переданному массиву строк
    public void paintGraphFromFile(ArrayList<String> matrix) throws IOException {
        graphView.getModel().beginUpdate();
        int numberElements = Integer.parseInt(matrix.get(0));
        for (int i = 1; i <= numberElements; i++) {
            String[] params = matrix.get(i).split("#");

            String type = params[0];
            String id = params[1];
            String value = params[2];
            int x = (int) Double.parseDouble(params[3]);
            int y = (int) Double.parseDouble(params[4]);

            Element element = new Vertex(type);
            element.getView().setId(id);
            element.saveNewValue(value);
            graphView.addMyCell(element, x, y);
            element.showValidatedText();
            elements.add(element);
        }

        for (int i = numberElements + 2; i < matrix.size(); i++) {

            String[] params = matrix.get(i).split("#");

            String type = params[0];
            String id = params[1];
            String value = params[2];
            String sourceId = params[3];
            String targetId = params[4];
            Object sourceObj = graphView.getVertexById(sourceId);
            Object targetObj = graphView.getVertexById(targetId);

            Element source = getElementByView(sourceObj);
            Element target = getElementByView(targetObj);

            Element element = new Edge(type);
            element.getView().setId(id);
            element.saveNewValue(value);
            graphView.addMyEdge(element, source, target);
            element.showValidatedText();
            elements.add(element);
        }
        graphView.getModel().endUpdate();
    }

    //--------------------------вспомогательные действия------------------------
    // Возвращает элемент по его представлению
    private Element getElementByView(Object view) {
        for (Element element : elements) {
            if (element.getView() == view) {
                return element;
            }
        }
        return null;
    }

    // Регистрирует новое представление как элемент 
    public void registerNewElement(mxCell obj, String style) {
        if (style != "arrow" && style != "dashArrow") {
            Vertex vertex = new Vertex(style);
            vertex.setView(obj);
            elements.add(vertex);
        } else {
            Edge edge = new Edge(style);
            edge.setView(obj);
            elements.add(edge);
        }
    }

    // Возвращает, есть ли пересечения у представления с существующими элементами-клетками
    public boolean checkCollisionsForCell(mxCell cell) {
        ArrayList<mxCell> list = new ArrayList<>();
        Rectangle rec = cell.getGeometry().getRectangle();
        for (Element element : getElements()) {
            if (element instanceof Vertex) {
                mxCell currentCell = (mxCell) element.getView();
                boolean intersection = currentCell.getGeometry().getRectangle().intersects(rec);
                if (intersection && currentCell != cell) {
                    list.add(currentCell);
                }
            }
        }
        return list.size() > 0;
    }
    
    // Обновляет положение всех стрелок на графе
    public void repaintAllArrows(){
        ArrayList<Element> newEdges = new ArrayList();
        for (int i = 0; i<elements.size(); i++){
            Element element = elements.get(i);
            if (element instanceof Edge){
                mxCell edgeView = element.getView();
                String elementType = edgeView.getStyle();
                mxCell sourceView = (mxCell) edgeView.getSource();
                mxCell targetView = (mxCell) edgeView.getTarget();
                Element edge = getElementByView(edgeView);
                graphView.getModel().beginUpdate();
                elements.remove(edge);
                i--;
                graphView.getModel().remove(edgeView);

                creatingElement = new Edge(elementType);
                Element source = getElementByView(sourceView);
                Element target = getElementByView(targetView);
                newEdges.add(creatingElement);
                graphView.addMyEdge(creatingElement, source, target);
                graphView.getModel().endUpdate();
                graphView.refresh();
                wasChanged = true;
            }
        }

        for (Element element : newEdges){
            elements.add(element);
        }
    }

    //--------------------------вспомогательные классы----------------
    // Обозреватель представления    
    class GraphViewObserver implements GraphViewListener {

        @Override
        // Реагирует на изменение положения клеток
        public void fireCellMoved(GraphViewEvent event) {
            // определить, возможно ли перемещение
            boolean canBeMoved = true;
            for (Object view : event.getMovingCells()) {
                mxCell cell = (mxCell) view;
                if (cell != null) {
                    boolean hasCollision = checkCollisionsForCell(cell);
                    if (hasCollision) {
                        canBeMoved = false;
                        myGraphModelListener.fireMessage("Наложение объектов недопустимо", "Ошибка");
                        break;
                    }
                }
            }

            //отметить изменения, если они есть
            if (canBeMoved) {
                setWasChanged(true);
            }

            //совершить пост-действия
            if (event.getIsClone()) {
                postCopyAction(event, canBeMoved);
            } else {                
                postMoveAction(event, canBeMoved);                
            }

        }

        // Осуществляет действия после переноса объектов
        private void postMoveAction(GraphViewEvent event, boolean canBeDone){
            if (!canBeDone) {
                //отменить изменения
                double dx = event.getDx();
                double dy = event.getDy();
                for (Object view : event.getMovingCells()) {                    
                    mxCell cell = (mxCell) view;
                    if (cell.isVertex()){
                        double x = cell.getGeometry().getX();
                        double y = cell.getGeometry().getY();
                        cell.getGeometry().setX(x - dx);
                        cell.getGeometry().setY(y - dy);
                    }                    
                }
            } 
            repaintAllArrows();
        }

        // Осуществляет действия после копирования объектов
        private void postCopyAction(GraphViewEvent event, boolean canBeDone) {
             graphView.removeCells(event.getMovingCells());
            if (canBeDone) {
                ArrayList <mxCell> edges = new ArrayList();
                Map <mxCell, Element> oldToNewConverter = new HashMap(); 
                for (Object obj : event.getMovingCells()) {
                    mxCell cell = (mxCell) obj;
                    if (cell.isVertex()) {
                        String style = cell.getStyle();
                        Element element = new Vertex(style);
                        double x = cell.getGeometry().getCenterX();
                        double y = cell.getGeometry().getCenterY();
                        graphView.addMyCell(element, x, y);
                        elements.add(element);
                        oldToNewConverter.put(cell, element);
                    }
                    else if(cell.isEdge()){
                        edges.add(cell);
                    }
                }
                for (mxCell cell :  edges){
                    mxCell sourceObj = (mxCell) cell.getSource();
                    mxCell targetObj = (mxCell) cell.getTarget();
                    String style = cell.getStyle();
                    Element element = new Edge(style);
                    Element source = oldToNewConverter.get(sourceObj);
                    Element target = oldToNewConverter.get(targetObj);
                    graphView.addMyEdge(element, source, target);
                    elements.add(element);
                }
                repaintAllArrows();
            }   
        }
    }

    // Слушатель изменения текста    
    class TextEditingListener implements mxIEventListener {

        @Override
        public void invoke(Object o, mxEventObject eo) {
            String value = (String) eo.getProperty("value");
            Object view = eo.getProperty("cell");
            Element element = getElementByView(view);
            if (((mxCell) view).getValue() != value) {
                wasChanged = true;
            }
            element.saveNewValue(value);
        }
    }

    // Слушатель начала редактирования текста
    class TextStartEditingListener implements mxIEventListener {

        @Override
        public void invoke(Object sender, mxEventObject eo) {
            mxCell cell = (mxCell) eo.getProperty("cell");
            if (cell == null) {
                return;
            }
            Element elem = getElementByView(cell);
            elem.showRealText();
        }
    }
}
