/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmpn;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;

/**
 *
 * @author trungchi321
 */
public class ButtonsController {

    private ArrayList<JButton> buttons = new ArrayList(); //управляемые кнопки
    private GraphModel graph;   //модель графа

    public ButtonsController(GraphModel graph) {
        this.graph = graph;
    }

    //добавить управляемую кнопку
    public void addButton(JButton button) {
        buttons.add(button);
    }

    //добавить слушателей кнопкам
    public void setListnersForButtons() {
        for (final JButton btn : buttons) {
            btn.setBackground(Color.lightGray);

            btn.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent me) {
                    if (me.getButton() == MouseEvent.BUTTON1) {
                        setButtonClicked(btn);
                    }
                }
            });
        }
    }
    
    //модель графа завершила действие
    public void actionDone() {
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setBackground(Color.lightGray);
        }
    }

    //нажать кнопку
    private void setButtonClicked(JButton btn) {

        if (btn.getBackground() == Color.BLUE) {
            btn.setBackground(Color.lightGray);
            graph.setAction("");
            return;
        }

        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setBackground(Color.lightGray);
        }

        btn.setBackground(Color.BLUE);
        String action = parseFromBtnToAction(btn);
        graph.setAction(action);
        String elementType = parseFromBtnToElement(btn);
        graph.setCreatingElementType(elementType);
    }

    //определить текущее действие по нажатой кнопке
    private String parseFromBtnToAction(JButton btn) {
        String result = "";
        String text = btn.getText();
        if (text.equalsIgnoreCase("Управления") || text.equalsIgnoreCase("Сообщений")) {
            result = "create edge";
        } else if (text.equalsIgnoreCase("Удалить")) {
            result = "delete";
        } else if (text.equals("Файл")) {
            result = "file";
        } else {
            result = "create element";
        }
        return result;
    }

    //определить добавляемый элемент по нажатой кнопке
    private String parseFromBtnToElement(JButton btn) {
        String result = "";
        switch (btn.getText()) {
            case "Стартовое":
                result = "start_event";
                break;
            case "Промежуточное":
                result = "intermediate_event";
                break;
            case "Конечное":
                result = "end_event";
                break;
            case "Задание":
                result = "task";
                break;
            case "Пул":
                result = "pool";
                break;
            case "Управления":
                result = "arrow";
                break;
            case "Сообщений":
                result = "dashArrow";
                break;
            case "Исключающее или данных":
                result = "databased";
                break;
            case "Исключающее или событий":
                result = "eventbased";
                break;
            case "Включающее или":
                result = "inclusive";
                break;
            case "Логическое и":
                result = "parallel";
                break;
            case "Стартовое сообщение":
                result = "MStart";
                break;
            case "Промежуточное сообщение":
                result = "MIntermediate";
                break;
            case "Конечное сообщение":
                result = "MEnd";
                break;
                
            default:
                result = "";
        }
        return result;
    }
}
