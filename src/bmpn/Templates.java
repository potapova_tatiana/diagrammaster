/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmpn;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxIGraphModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс шаблонов
 *
 * @author kynew
 */
public class Templates {

    private GraphModel graph;                  // диаграмма 
    private Element creatingElement;           // создаваемый элемент
    private Object view;                       // view, создаваемого элемента
    private Element first, second, third, fourth; // элементы, которые используются при создании шаблонов
    private ArrayList<Element> tempElements = new ArrayList(); // массив созданных элементов
    private int x = 10, y = 100, delta = 130, deltaTask = 150;

    Templates(GraphModel graph) {
        this.graph = graph;
    }

    public boolean addSequenceFlow() {
        x = (int) findLastRightX() + delta;
        first = createElement("start_event", x, y);
        x = x + delta;
        second = createElement("task", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + deltaTask;
        second = createElement("task", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("end_event", x, y);
        createEdge("arrow", first, second);
        if (!validTemplate()) {
            deleteTemplate();
            return false;
        }
        tempElements.clear();
        return true;
    }

    public boolean addParallelSplit() {
        x = (int) findLastRightX() + delta;
        first = createElement("start_event", x, y);
        x = x + delta;
        second = createElement("task", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("parallel", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("task", x, y - 45);
        createEdge("arrow", first, second);
        third = createElement("task", x, y + 45);
        createEdge("arrow", first, third);
        first = second;
        x = x + delta;
        second = createElement("end_event", x, y - 45);
        createEdge("arrow", first, second);
        second = createElement("end_event", x, y + 45);
        createEdge("arrow", third, second);
        if (!validTemplate()) {
            deleteTemplate();
            return false;
        }
        tempElements.clear();
        return true;
    }

    public boolean addSynchronization() {
        x = (int) findLastRightX() + delta;
        first = createElement("start_event", x, y);
        x = x + delta;
        second = createElement("task", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("parallel", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("task", x, y - 45);
        createEdge("arrow", first, second);
        third = createElement("task", x, y + 45);
        createEdge("arrow", first, third);
        first = second;
        x = x + delta;
        second = createElement("parallel", x, y);
        createEdge("arrow", first, second);
        createEdge("arrow", third, second);
        first = second;
        x = x + delta;
        second = createElement("task", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("end_event", x, y);

        createEdge("arrow", first, second);
        if (!validTemplate()) {
            deleteTemplate();
            return false;
        }
        tempElements.clear();
        return true;
    }

    public boolean addMultipleChoice() {
        x = (int) findLastRightX() + delta;
        first = createElement("start_event", x, y);
        x = x + delta;
        second = createElement("task", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("inclusive", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("task", x, y - 45);
        createEdge("arrow", first, second);
        third = createElement("task", x, y);
        createEdge("arrow", first, third);
        fourth = createElement("task", x, y + 45);
        createEdge("arrow", first, fourth);
        x = x + delta;
        first = createElement("end_event", x, y - 45);
        createEdge("arrow", second, first);
        first = createElement("end_event", x, y);
        createEdge("arrow", third, first);
        first = createElement("end_event", x, y + 45);
        createEdge("arrow", fourth, first);
        if (!validTemplate()) {
            deleteTemplate();
            return false;
        }
        tempElements.clear();
        return true;
    }

    public boolean addMultipleMerge() {
        x = (int) findLastRightX() + delta;
        first = createElement("start_event", x, y);
        x = x + delta;
        second = createElement("parallel", x, y);
        createEdge("arrow", first, second);
        first = second;
        x = x + delta;
        second = createElement("task", x, y - 45);
        createEdge("arrow", first, second);
        third = createElement("task", x, y + 45);
        createEdge("arrow", first, third);
        x = x + deltaTask;
        first = createElement("task", x, y);
        createEdge("arrow", second, first);
        createEdge("arrow", third, first);
        x = x + delta;
        second = createElement("end_event", x, y);
        createEdge("arrow", first, second);
        if (!validTemplate()) {
            deleteTemplate();
            return false;
        }
        tempElements.clear();
        return true;
    }

    public boolean addExclusiveGateway(int x, int y) {
        first = createElement("databased", x, y);
        second = createElement("task", x + 85, y - 65);
        third = createElement("task", x + 85, y);
        fourth = createElement("task", x + 85, y + 65);
        createEdge("arrow", first, second);
        createEdge("arrow", first, third);
        createEdge("arrow", first, fourth);
        if (!validTemplate()) {
            deleteTemplate();
            return false;
        }
        tempElements.clear();
        return true;
    }

    // Создает элемент заданного типа в позиции х и у , возвращает ссылку на элемент
    private Element createElement(String type, int x, int y) {
        creatingElement = new Vertex(type);
        view = graph.getGraphView().addMyCell(creatingElement, x, y);
        // Проверка на пересечение с другими элементами диаграммы
        if (graph.checkCollisionsForCell((mxCell) view)) {
            Object[] cells = new Object[1];
            cells[0] = view;
            graph.getGraphView().removeCells(cells);
            tempElements.add(null);
            return null;
        }
        graph.getElements().add(creatingElement);
        graph.setWasChanged(true);
        tempElements.add(creatingElement);
        return creatingElement;
    }

    // Создает стрелку заданного типа между двумя элементами
    private boolean createEdge(String type, Element first, Element second) {
        if (first == null || second == null) { // Если до этого произошел сбой в создании элементов - выходим
            return false;
        }
        creatingElement = new Edge(type);        
        Object edgeView = graph.getGraphView().addMyEdge(creatingElement, first, second);        
        graph.getElements().add(creatingElement);
        graph.setWasChanged(true);
        tempElements.add(creatingElement);
        return true;
    }

    // Находим самую правую Х-координату элемента диаграммы
    private double findLastRightX() {
        double x = 0;
        for (Element element : graph.getElements()) {
            if (x < ((mxCell) element.getView()).getGeometry().getX()) {
                x = ((mxCell) element.getView()).getGeometry().getX();
            }
        }
        if (x == 0) {
            x = 10;
        }
        return x;
    }

    // Проверка на наложение элементов шаблона на другие элементы диаграммы, если нет наложения - true, иначе -  false
    private boolean validTemplate() {
        for (Element element : tempElements) {
            if (element == null || graph.checkCollisionsForCell((mxCell) element.getView())) {
                return false;
            }
        }
        return true;
    }

    // Удаляем шаблон из диаграммы
    private void deleteTemplate() {
        graph.getGraphView().getModel().beginUpdate();
        for (Element element : tempElements) {
            if (element != null) {
                for (int i = 0; i < graph.getElements().size(); ++i) {
                    if (element.getView().equals(graph.getElements().get(i).getView())) {
                        graph.getGraphView().getModel().remove(graph.getElements().get(i).getView());
                        graph.getElements().remove(graph.getElements().get(i));
                    }
                }
            }
        }
        graph.getMyGraphModelListener().fireMessage("Нельзя добавить элемент в это место", "Ошибка");
        System.out.print("шаблон не поместился \n");
        graph.getGraphView().getModel().endUpdate();
        tempElements.clear();
    }
}
