/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bmpn;

import Events.GraphViewListener;
import Events.MyGraphModelListener;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.view.mxGraph;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * Главное окно программы
 *
 * @author Tatiana
 */
public class MainFrame extends javax.swing.JFrame {

    ArrayList<JButton> buttons = new ArrayList();   //набор кнопок
    mxGraphComponent graphComponent;                //компонент графа
    GraphModel graph = new GraphModel();            //модель графа
    ButtonsController buttonsController = new ButtonsController(graph); //контроллер кнопок
    FileManager fileManager = new FileManager(this);    //файл менеджер
    Templates template = new Templates(graph);          //менеджер шаблонов

    Settings settings = new Settings(this);         //настройки

    // Конструктор
    public MainFrame() {
        initComponents();
        setIcons();
        addButtons();
        createGraphPlaneInPanel();
        setLocationRelativeTo(null);
        updateLastOpenedFiles();
        String mail = fileManager.getEmail();
        settings.setEmail(mail);
    }

    // Возвращает менеджер файлов
    public FileManager getFileManager() {
        return fileManager;
    }

    // Устанавливает иконки рядом с кнопками
    private void setIcons() {

        //ImageIcon icon = new ImageIcon("/start.png");
        ImageIcon icon = new ImageIcon(this.getClass().getResource("/images/start.png"));
        Image img = icon.getImage();
        Image newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbStart.setIcon(new ImageIcon(newimg));
        lbStart.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/intermediate.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbInter.setIcon(new ImageIcon(newimg));
        lbInter.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/end.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbEnd.setIcon(new ImageIcon(newimg));
        lbEnd.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/task.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 20, java.awt.Image.SCALE_SMOOTH);
        lbTask.setIcon(new ImageIcon(newimg));
        lbTask.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/pool.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 10, java.awt.Image.SCALE_SMOOTH);
        lbPool.setIcon(new ImageIcon(newimg));
        lbPool.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/sequenseFlow.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbSeque.setIcon(new ImageIcon(newimg));
        lbSeque.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/messflow.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbMess.setIcon(new ImageIcon(newimg));
        lbMess.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/databased.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbData.setIcon(new ImageIcon(newimg));
        lbData.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/eventbased.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbEvent.setIcon(new ImageIcon(newimg));
        lbEvent.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/inclusive.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbInclus.setIcon(new ImageIcon(newimg));
        lbInclus.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/parallel.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbPara.setIcon(new ImageIcon(newimg));
        lbPara.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/msgStart.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbStartMess.setIcon(new ImageIcon(newimg));
        lbStartMess.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/msgIntermediate.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbInterMess.setIcon(new ImageIcon(newimg));
        lbInterMess.setText("");

        icon = new ImageIcon(this.getClass().getResource("/images/msgEnd.png"));
        img = icon.getImage();
        newimg = img.getScaledInstance(30, 30, java.awt.Image.SCALE_SMOOTH);
        lbEndMess.setIcon(new ImageIcon(newimg));
        lbEndMess.setText("");

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu3 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        jPopupMenu3 = new javax.swing.JPopupMenu();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnStart = new javax.swing.JButton();
        lbStart = new javax.swing.JLabel();
        lbInter = new javax.swing.JLabel();
        btnIntermediate = new javax.swing.JButton();
        btnEnd = new javax.swing.JButton();
        lbEnd = new javax.swing.JLabel();
        btnMEnd = new javax.swing.JButton();
        btnMIntermediate = new javax.swing.JButton();
        btnMStart = new javax.swing.JButton();
        lbStartMess = new javax.swing.JLabel();
        lbInterMess = new javax.swing.JLabel();
        lbEndMess = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        btnParallel = new javax.swing.JButton();
        btnInclusive = new javax.swing.JButton();
        btnEventbased = new javax.swing.JButton();
        btnDatabase = new javax.swing.JButton();
        lbData = new javax.swing.JLabel();
        lbEvent = new javax.swing.JLabel();
        lbInclus = new javax.swing.JLabel();
        lbPara = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        btnArrow2 = new javax.swing.JButton();
        btnArrow = new javax.swing.JButton();
        lbMess = new javax.swing.JLabel();
        lbSeque = new javax.swing.JLabel();
        btnDelete = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        btnPool = new javax.swing.JButton();
        btnTask = new javax.swing.JButton();
        lbPool = new javax.swing.JLabel();
        lbTask = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        createMenuitem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        openMenuitem = new javax.swing.JMenuItem();
        openLastMenu = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        saveMenuitem1 = new javax.swing.JMenuItem();
        saveAsMenuitem = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        printMenuitem = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        exportMenuitem = new javax.swing.JMenuItem();
        jSettings = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        jMenu3.setText("jMenu3");

        jMenu1.setText("jMenu1");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(600, 500));

        jPanel1.setMinimumSize(new java.awt.Dimension(0, 0));
        jPanel1.setPreferredSize(new java.awt.Dimension(200, 100));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 545, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        btnStart.setText("Стартовое");

        lbStart.setText("jLabel1");
        lbStart.setMaximumSize(new java.awt.Dimension(30, 30));
        lbStart.setMinimumSize(new java.awt.Dimension(30, 30));
        lbStart.setPreferredSize(new java.awt.Dimension(30, 30));

        lbInter.setText("jLabel1");
        lbInter.setMaximumSize(new java.awt.Dimension(30, 30));
        lbInter.setMinimumSize(new java.awt.Dimension(30, 30));
        lbInter.setPreferredSize(new java.awt.Dimension(30, 30));

        btnIntermediate.setText("Промежуточное");

        btnEnd.setText("Конечное");

        lbEnd.setText("jLabel1");
        lbEnd.setMaximumSize(new java.awt.Dimension(30, 30));
        lbEnd.setMinimumSize(new java.awt.Dimension(30, 30));
        lbEnd.setPreferredSize(new java.awt.Dimension(30, 30));

        btnMEnd.setText("Конечное сообщение");

        btnMIntermediate.setText("Промежуточное сообщение");

        btnMStart.setText("Стартовое сообщение");

        lbStartMess.setText("jLabel1");
        lbStartMess.setMaximumSize(new java.awt.Dimension(30, 30));
        lbStartMess.setMinimumSize(new java.awt.Dimension(30, 30));
        lbStartMess.setPreferredSize(new java.awt.Dimension(30, 30));

        lbInterMess.setText("jLabel1");
        lbInterMess.setMaximumSize(new java.awt.Dimension(30, 30));
        lbInterMess.setMinimumSize(new java.awt.Dimension(30, 30));
        lbInterMess.setPreferredSize(new java.awt.Dimension(30, 30));

        lbEndMess.setText("jLabel1");
        lbEndMess.setMaximumSize(new java.awt.Dimension(30, 30));
        lbEndMess.setMinimumSize(new java.awt.Dimension(30, 30));
        lbEndMess.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbInter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnEnd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnIntermediate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnStart, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbStartMess, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbInterMess, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbEndMess, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnMEnd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnMStart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnMIntermediate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(0, 10, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnStart)
                    .addComponent(lbStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnIntermediate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbInter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEnd, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMStart)
                    .addComponent(lbStartMess, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMIntermediate)
                    .addComponent(lbInterMess, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMEnd)
                    .addComponent(lbEndMess, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnEnd, btnIntermediate, btnMEnd, btnMIntermediate, btnMStart, btnStart});

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 27, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(jPanel2);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Потоки");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel2.setText("События");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel3.setText("Действия");

        btnParallel.setText("Логическое и");

        btnInclusive.setText("Включающее или");
        btnInclusive.setToolTipText("");

        btnEventbased.setText("Исключающее или событий");

        btnDatabase.setText("Исключающее или данных");
        btnDatabase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDatabaseActionPerformed(evt);
            }
        });

        lbData.setText("jLabel1");
        lbData.setMaximumSize(new java.awt.Dimension(30, 30));
        lbData.setMinimumSize(new java.awt.Dimension(30, 30));
        lbData.setPreferredSize(new java.awt.Dimension(30, 30));

        lbEvent.setText("jLabel1");
        lbEvent.setMaximumSize(new java.awt.Dimension(30, 30));
        lbEvent.setMinimumSize(new java.awt.Dimension(30, 30));
        lbEvent.setPreferredSize(new java.awt.Dimension(30, 30));

        lbInclus.setText("jLabel1");
        lbInclus.setMaximumSize(new java.awt.Dimension(30, 30));
        lbInclus.setMinimumSize(new java.awt.Dimension(30, 30));
        lbInclus.setPreferredSize(new java.awt.Dimension(30, 30));

        lbPara.setText("jLabel1");
        lbPara.setMaximumSize(new java.awt.Dimension(30, 30));
        lbPara.setMinimumSize(new java.awt.Dimension(30, 30));
        lbPara.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbData, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbEvent, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbInclus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPara, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnInclusive, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEventbased, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                    .addComponent(btnDatabase, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnParallel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDatabase)
                    .addComponent(lbData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEventbased)
                    .addComponent(lbEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInclusive)
                    .addComponent(lbInclus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbPara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnParallel))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        jScrollPane4.setViewportView(jPanel9);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel4.setText("Логические операторы");

        btnArrow2.setText("Сообщений");

        btnArrow.setText("Управления");

        lbMess.setText("jLabel1");
        lbMess.setMaximumSize(new java.awt.Dimension(30, 30));
        lbMess.setMinimumSize(new java.awt.Dimension(30, 30));
        lbMess.setPreferredSize(new java.awt.Dimension(30, 30));

        lbSeque.setText("jLabel1");
        lbSeque.setMaximumSize(new java.awt.Dimension(30, 30));
        lbSeque.setMinimumSize(new java.awt.Dimension(30, 30));
        lbSeque.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbSeque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbMess, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnArrow, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                    .addComponent(btnArrow2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnArrow)
                    .addComponent(lbSeque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbMess, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnArrow2))
                .addGap(0, 17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        jScrollPane5.setViewportView(jPanel11);

        btnDelete.setText("Удалить");

        btnPool.setText("Пул");

        btnTask.setText("Задание");

        lbPool.setText("jLabel1");
        lbPool.setMaximumSize(new java.awt.Dimension(30, 30));
        lbPool.setMinimumSize(new java.awt.Dimension(30, 30));
        lbPool.setPreferredSize(new java.awt.Dimension(30, 30));

        lbTask.setText("jLabel1");
        lbTask.setMaximumSize(new java.awt.Dimension(30, 30));
        lbTask.setMinimumSize(new java.awt.Dimension(30, 30));
        lbTask.setPreferredSize(new java.awt.Dimension(30, 30));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPool, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTask, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                    .addComponent(btnPool, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTask)
                    .addComponent(lbTask, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbPool, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPool))
                .addGap(0, 17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        jScrollPane6.setViewportView(jPanel13);

        jMenu2.setText("Файл");

        createMenuitem.setText("Создать");
        createMenuitem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createMenuitemActionPerformed(evt);
            }
        });
        jMenu2.add(createMenuitem);
        jMenu2.add(jSeparator1);

        openMenuitem.setText("Открыть ...");
        openMenuitem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuitemActionPerformed(evt);
            }
        });
        jMenu2.add(openMenuitem);

        openLastMenu.setText("Открыть из последних");
        jMenu2.add(openLastMenu);
        jMenu2.add(jSeparator2);

        saveMenuitem1.setText("Сохранить");
        saveMenuitem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuitem1ActionPerformed(evt);
            }
        });
        jMenu2.add(saveMenuitem1);

        saveAsMenuitem.setText("Сохранить как ...");
        saveAsMenuitem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuitemActionPerformed(evt);
            }
        });
        jMenu2.add(saveAsMenuitem);
        jMenu2.add(jSeparator3);

        printMenuitem.setText("Печать");
        printMenuitem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printMenuitemActionPerformed(evt);
            }
        });
        jMenu2.add(printMenuitem);
        jMenu2.add(jSeparator4);

        exportMenuitem.setText("Экспорт в картинку ...");
        exportMenuitem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportMenuitemActionPerformed(evt);
            }
        });
        jMenu2.add(exportMenuitem);

        jMenuBar1.add(jMenu2);

        jSettings.setLabel("Настройки");
        jSettings.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                settingsMouseClicked(evt);
            }
        });
        jMenuBar1.add(jSettings);

        jMenu4.setText("Шаблоны");

        jMenuItem3.setText("Последовательный поток");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem3);

        jMenuItem2.setText("Параллельный поток");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem2);

        jMenuItem4.setText("Синхронизация");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem4);

        jMenuItem5.setText("Множественный выбор");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem5);

        jMenuItem6.setText("Множественное объединение");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem6);

        jMenuBar1.add(jMenu4);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDelete))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 664, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Отрывает файл
    private void openMenuitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuitemActionPerformed
        try {
            // TODO add your handling code here:
            BufferedImage image = mxCellRenderer.createBufferedImage(graphComponent.getGraph(),
                    null, 1, Color.WHITE, graphComponent.isAntiAlias(), null, graphComponent.getCanvas());
            
            if (image != null && graph.getWasChanged()) {
                Object[] options = {"Да", "Нет"};
                int n = JOptionPane.showOptionDialog(this,
                        "Сохранить текущую диаграмму?", "Подтверждение",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                
                if (n == 0) {
                    fileManager.saveGraphToCurrentFile(graph);
                } else if (n == -1) {
                    return;
                }
            }
            fileManager.loadGraphFromFile(graph, null);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_openMenuitemActionPerformed

    // Сохраняет диаграмму как
    private void saveAsMenuitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuitemActionPerformed
        // TODO add your handling code here:
        fileManager.saveAsGraphToFile(graph);
    }//GEN-LAST:event_saveAsMenuitemActionPerformed

    // Экспортирует диаграмму в картинку
    private void exportMenuitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportMenuitemActionPerformed
        // TODO add your handling code here:
        int rows = settings.getRows();
        int cols = settings.getCols();
        boolean isActive = settings.getIsActive();
        fileManager.exportImage(graphComponent, isActive, rows, cols);
    }//GEN-LAST:event_exportMenuitemActionPerformed

    // Создает новую диаграмму
    private void createMenuitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createMenuitemActionPerformed
        // TODO add your handling code here:
        BufferedImage image = mxCellRenderer.createBufferedImage(graphComponent.getGraph(),
                null, 1, Color.WHITE, graphComponent.isAntiAlias(), null, graphComponent.getCanvas());

        if (image != null) {
            Object[] options = {"Да", "Нет"};
            if (fileManager.getCurrentFile() != null) {
                int n = JOptionPane.showOptionDialog(this,
                        "Сохранить текущую диаграммму как \"" + fileManager.getCurrentFile().getName() + "\"?", "Подтверждение",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (n == 0) {
                    fileManager.saveGraphToCurrentFile(graph);
                } else if (n == -1) {
                    return;
                }
            } else {
                int n = JOptionPane.showOptionDialog(this,
                        "Сохранить текущую диаграммму?", "Подтверждение",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (n == 0) {
                    fileManager.saveAsGraphToFile(graph);
                } else if (n == -1) {
                    return;
                }
            }
        }
        graph.clearAll();
        fileManager.setCurrentFile(null);
    }//GEN-LAST:event_createMenuitemActionPerformed

    // Сохраняет текущую диаграмму
    private void saveMenuitem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuitem1ActionPerformed
        // TODO add your handling code here:
        fileManager.saveGraphToCurrentFile(graph);

    }//GEN-LAST:event_saveMenuitem1ActionPerformed

    // Вызывает настройки
    private void settingsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_settingsMouseClicked
        // TODO add your handling code here:  
        settings.setValues();
        setEnabled(false);
        settings.setVisible(true);
    }//GEN-LAST:event_settingsMouseClicked

    // Выводит на печать
    private void printMenuitemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printMenuitemActionPerformed
        // TODO add your handling code here:
        PrinterJob pjob = PrinterJob.getPrinterJob();

        // Устанавливаем задание для печати.        
        PageFormat pageFormat = graphComponent.getPageFormat();
        pjob.setPrintable(graphComponent, pageFormat);
        pjob.setJobName("BMPN diagramm");

        if (pjob.printDialog()) {
            try {
                // Непосредственно печатаем
                pjob.print();
            } catch (PrinterException pe) {
                System.out.println("Error printing: " + pe);
            }
        }

    }//GEN-LAST:event_printMenuitemActionPerformed

    //------------------------------Шаблоны-------------------------------------
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        template.addSequenceFlow();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        template.addMultipleMerge();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        template.addMultipleChoice();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        template.addSynchronization();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        template.addParallelSplit();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void btnDatabaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDatabaseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnDatabaseActionPerformed

    // Добавляет кнопки в менеджер кнопок
    private void addButtons() {
        buttonsController.addButton(btnStart);
        buttonsController.addButton(btnIntermediate);
        buttonsController.addButton(btnEnd);
        buttonsController.addButton(btnTask);
        buttonsController.addButton(btnPool);
        buttonsController.addButton(btnArrow);
        buttonsController.addButton(btnArrow2);
        buttonsController.addButton(btnDatabase);
        buttonsController.addButton(btnEventbased);
        buttonsController.addButton(btnInclusive);
        buttonsController.addButton(btnParallel);
        buttonsController.addButton(btnMStart);
        buttonsController.addButton(btnMIntermediate);
        buttonsController.addButton(btnMEnd);
        buttonsController.addButton(btnDelete);

        buttonsController.setListnersForButtons();
    }

    // Создает и добавляет облась для построения графа
    private void createGraphPlaneInPanel() {
        mxGraph graphView = graph.getGraphView();

        graph.setController(buttonsController);
        graph.setMyGraphModelListener(new GraphModelObserver());

        graphComponent = new mxGraphComponent(graphView);
        graphComponent.setConnectable(false);
        graphComponent.getViewport().setBackground(Color.WHITE);

        graph.setGraphComponent(graphComponent);

        graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent me) {
                if (me.getButton() == MouseEvent.BUTTON1) {
                    try {
                        graph.mousePressed(me.getX(), me.getY());
                    } catch (IOException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                        System.err.println("error with mousePressed");
                    }
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                System.err.println("Mouse dragged");
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                System.err.println("Mouse moved");
            }

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                System.err.println("Mouse mouseWheelMoved");
            }
        });

        graphComponent.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent evt) {
                graph.getGraphView().reSize(graphComponent.getWidth(), graphComponent.getHeight());
            }
        });

        graphComponent.setAutoScroll(true);
        graphComponent.setAutoscrolls(true);
        graphComponent.setAutoExtend(true);
        graphComponent.setAntiAlias(true);

        graphComponent.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent e) {

                System.out.println("keyTyped");
            }

            @Override
            public void keyPressed(KeyEvent e) {

                System.out.println("keyPressed");

                if (e.getKeyCode() == 127) {
                    graph.deleteElements();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                System.out.println("keyReleased");
            }

        });

        jPanel1.setBackground(Color.WHITE);
        jPanel1.setLayout(new BoxLayout(jPanel1, BoxLayout.LINE_AXIS));
        graphComponent.setMinimumSize(new Dimension(400, 400));
        jPanel1.setMinimumSize(new Dimension(400, 400));
        jPanel1.add(graphComponent);
        pack();
        jPanel1.setVisible(true);
    }

    // Обновляет список последних файлов
    public void updateLastOpenedFiles() {
        ArrayList<String> lastFiles = fileManager.getLastFiveOpenedFiles();
        openLastMenu.removeAll();
        for (final String lastFile : lastFiles) {
            JMenuItem menuItem = new JMenuItem(lastFile);
            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        fileManager.loadGraphFromFile(graph, lastFile);
                    } catch (IOException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            openLastMenu.add(menuItem);
        }
    }

    // Обозреватель модели графа 
    class GraphModelObserver implements MyGraphModelListener {

        @Override
        public void fireMessage(String message, String type) {
            showMessage(message, type);
        }
    }

    // Выводит сообщения
    private void showMessage(String message, String type) {
        JOptionPane.showMessageDialog(this, message, type, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnArrow;
    private javax.swing.JButton btnArrow2;
    private javax.swing.JButton btnDatabase;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEnd;
    private javax.swing.JButton btnEventbased;
    private javax.swing.JButton btnInclusive;
    private javax.swing.JButton btnIntermediate;
    private javax.swing.JButton btnMEnd;
    private javax.swing.JButton btnMIntermediate;
    private javax.swing.JButton btnMStart;
    private javax.swing.JButton btnParallel;
    private javax.swing.JButton btnPool;
    private javax.swing.JButton btnStart;
    private javax.swing.JButton btnTask;
    private javax.swing.JMenuItem createMenuitem;
    private javax.swing.JMenuItem exportMenuitem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JPopupMenu jPopupMenu3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JMenu jSettings;
    private javax.swing.JLabel lbData;
    private javax.swing.JLabel lbEnd;
    private javax.swing.JLabel lbEndMess;
    private javax.swing.JLabel lbEvent;
    private javax.swing.JLabel lbInclus;
    private javax.swing.JLabel lbInter;
    private javax.swing.JLabel lbInterMess;
    private javax.swing.JLabel lbMess;
    private javax.swing.JLabel lbPara;
    private javax.swing.JLabel lbPool;
    private javax.swing.JLabel lbSeque;
    private javax.swing.JLabel lbStart;
    private javax.swing.JLabel lbStartMess;
    private javax.swing.JLabel lbTask;
    private javax.swing.JMenu openLastMenu;
    private javax.swing.JMenuItem openMenuitem;
    private javax.swing.JMenuItem printMenuitem;
    private javax.swing.JMenuItem saveAsMenuitem;
    private javax.swing.JMenuItem saveMenuitem1;
    // End of variables declaration//GEN-END:variables
}
