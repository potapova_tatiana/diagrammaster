package bmpn;

import Events.GraphViewEvent;
import Events.GraphViewListener;
import FindPath.Matrix;
import FindPath.PathFinding;
import FindPath.Point;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxRectangle;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Представление графа
 *
 * @author Tatiana
 */
public class GraphView extends mxGraph {

    private final Object parent = this.getDefaultParent(); //родитель
    private GraphViewListener graphViewListener;           //слушатель представления

    private Matrix matrix;
    private int WIDTH = 0;
    private int HEIGHT = 0;

    // Слушатель перемещения клеток
    mxIEventListener MoveCellsListener = new mxIEventListener() {
        @Override
        public void invoke(Object o, mxEventObject eo) {
            //подготовить сигнал
            Object[] os = (Object[]) (eo.getProperty("cells"));
            boolean isClone = (boolean) (eo.getProperty("clone"));
            double dx = (double) eo.getProperty("dx");
            double dy = (double) eo.getProperty("dy");
            GraphViewEvent event = new GraphViewEvent(this);
            event.setMovingCells(os);
            event.setDx(dx);
            event.setDy(dy);
            event.setIsClone(isClone);
            //отправить сигнал на обработку перемещения
            graphViewListener.fireCellMoved(event);
        }
    };

    // Конструктор
    public GraphView() {
        initialiseStyles();
        setAllowDanglingEdges(false);
        setAutoOrigin(true);
        setAllowNegativeCoordinates(true);
        setAllowLoops(true);
        addListener(mxEvent.MOVE_CELLS, MoveCellsListener);
        matrix = new Matrix(this);
    }

    // Задает слушателя представления
    public void setGraphViewListener(GraphViewListener listener) {
        graphViewListener = listener;
    }

    public void reSize(int width, int height) {
        WIDTH = width;
        HEIGHT = height;
    }

    // Добавляет стрелку
    public Object addMyEdge(Element edge, Element source, Element target) {

        if (findLastRightX() > WIDTH) {
            WIDTH = (int) findLastRightX();
        }

        matrix.setSize(WIDTH, HEIGHT);

        getModel().beginUpdate();
        mxCell mxEdge = (mxCell) this.addEdge(edge.getView(), getDefaultParent(), source.getView(), target.getView(), null);
        mxEdge.setGeometry(new mxGeometry());
        mxGeometry geo = mxEdge.getGeometry();
        getModel().endUpdate();

        getView().reload();

        if (source.getView().equals(target.getView())) {
            return mxEdge;
        }

        mxCellState stateEdge = getView().getState(mxEdge);

        List<mxPoint> points = stateEdge.getAbsolutePoints();

        geo.setSourcePoint(points.get(0));
        geo.setTargetPoint(points.get(1));

        int y1 = (int) points.get(0).getX() / Matrix.SCALE;
        int x1 = (int) points.get(0).getY() / Matrix.SCALE;
        int y2 = (int) points.get(points.size() - 1).getX() / Matrix.SCALE;
        int x2 = (int) points.get(points.size() - 1).getY() / Matrix.SCALE;

        if (matrix.getMatrix()[y1][x1] == 0) {

            if (x1 > x2) {
                while (matrix.getMatrix()[y1][x1] == 0) {
                    x1--;
                }
            } else {
                while (matrix.getMatrix()[y1][x1] == 0) {
                    x1++;
                }
            }
        }

        if (matrix.getMatrix()[y2][x2] == 0) {

            if (x2 > x1) {
                while (matrix.getMatrix()[y2][x2] == 0) {
                    x2--;
                }
            } else {
                while (matrix.getMatrix()[y2][x2] == 0) {
                    x2++;
                }
            }
        }

        PathFinding finding = new PathFinding(matrix.getMatrix(), new Point(x2, y2));
        List<Point> nodes = finding.compute(new Point(x1, y1));

        getModel().beginUpdate();
        
        points.addAll(1, convertNodeTomxPoints(nodes));
        geo.setPoints(convertNodeTomxPoints(nodes));

        getModel().endUpdate();

        return mxEdge;
    }

    private double findLastRightX() {
        double x = 0;

        Object[] elements = getChildCells(parent, false, false);

        for (Object element : elements) {
            if (x < ((mxCell) element).getGeometry().getX()) {
                x = ((mxCell) element).getGeometry().getX();
            }
        }
        if (x == 0) {
            x = 10;
        }

        return x + 130;
    }

    public ArrayList<mxPoint> convertNodeTomxPoints(List<Point> nodes) {
        ArrayList<mxPoint> points = new ArrayList<>();

        for (Point node : nodes) {
            points.add(new mxPoint(node.y * Matrix.SCALE, node.x * Matrix.SCALE));
        }

        return points;
    }

    // Добавляет клетку
    public Object addMyCell(Element element, double x, double y) {
        double width = element.getView().getGeometry().getWidth();
        double height = element.getView().getGeometry().getHeight();
        double dx = 0;
        double dy = 0;
        if (x - width / 2 < 0) {
            dx = width / 2;
            x += width / 2;
        }
        if (y - height / 2 < 0) {
            dy = height / 2;
            y += height / 2;
        }
        moveAllElements(dx, dy);
        getModel().beginUpdate();
        Object v1;
        try {
            ((mxCell) element.getView()).getGeometry().setX(x - width / 2);
            ((mxCell) element.getView()).getGeometry().setY(y - height / 2);
            v1 = addCell((mxCell) element.getView(), null);


        } finally {
            getModel().endUpdate();
        }
        return v1;
    }

    // Возвращает представление элемента по id
    public Object getVertexById(String id) {
        Object[] cells = getChildCells(parent, true, false);

        mxCell temp = null;

        for (Object cell : cells) {
            temp = (mxCell) cell;

            if (temp.getId().equalsIgnoreCase(id)) {
                return temp;
            }
        }

        return temp;
    }

    // Возвражает представление элемента на заданной позиции
    public Object getElementByXY(int x, int y) {
        Object[] cells = getChildEdges(parent);
        Object[] mass = getChildCells(parent, true, true);
        Object result = null;

        for (Object mas : mass) {
            mxCell cell = (mxCell) mas;
            if (cell.getGeometry().contains(x, y)) {
                result = mas;
                break;
            }
        }
        return result;
    }

    // Инициализирует стили, учавствующие в графе
    private void initialiseStyles() {

        mxStylesheet stylesheet1 = this.getStylesheet();

        stylesheet1.putCellStyle("start_event", createStyleForIconElementWithELLIPSE("/images/start.png"));
        stylesheet1.putCellStyle("intermediate_event", createStyleForIconElementWithELLIPSE("/images/intermediate.png"));
        stylesheet1.putCellStyle("end_event", createStyleForIconElementWithELLIPSE("/images/end.png"));
        stylesheet1.putCellStyle("databased", createStyleForIconElementWithRHOMNUS("/images/databased.png"));
        stylesheet1.putCellStyle("eventbased", createStyleForIconElementWithRHOMNUS("/images/eventbased.png"));
        stylesheet1.putCellStyle("inclusive", createStyleForIconElementWithRHOMNUS("/images/inclusive.png"));
        stylesheet1.putCellStyle("parallel", createStyleForIconElementWithRHOMNUS("/images/parallel.png"));
        stylesheet1.putCellStyle("MStart", createStyleForIconElementWithELLIPSE("/images/msgStart.png"));
        stylesheet1.putCellStyle("MIntermediate", createStyleForIconElementWithELLIPSE("/images/msgIntermediate.png"));
        stylesheet1.putCellStyle("MEnd", createStyleForIconElementWithELLIPSE("/images/msgEnd.png"));

        Hashtable<String, Object> style = new Hashtable<String, Object>();
        style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        style.put(mxConstants.STYLE_ROUNDED, true);
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_FONTSIZE, "12");
        style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        style.put(mxConstants.STYLE_STROKECOLOR, "#000000");
        style.put(mxConstants.STYLE_FILLCOLOR, "#ffffff");
        style.put(mxConstants.STYLE_WHITE_SPACE, "wrap");
        style.put(mxConstants.STYLE_RESIZABLE, "0");
        stylesheet1.putCellStyle("task", style);

        style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        style.put(mxConstants.STYLE_ROUNDED, false);
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_FONTSIZE, "12");
        style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        style.put(mxConstants.STYLE_STROKECOLOR, "#000000");
        style.put(mxConstants.STYLE_FILLCOLOR, "#ffffff");
        style.put(mxConstants.STYLE_WHITE_SPACE, "wrap");
        style.put(mxConstants.STYLE_RESIZABLE, "0");
        stylesheet1.putCellStyle("pool", style);

        style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CONNECTOR);
        //edgeArrow.put(mxConstants.STYLE_ROUNDED, true);
        style.put(mxConstants.STYLE_FONTSIZE, "15");
        style.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_STROKECOLOR, "#000000"); // default is #6482B9
        style.put(mxConstants.STYLE_FONTCOLOR, "#446299");
        style.put(mxConstants.STYLE_STARTARROW, mxConstants.NONE);
        style.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_BLOCK);
        style.put(mxConstants.STYLE_EDITABLE, "1");
        //style.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_SEGMENT);
        style.put(mxConstants.STYLE_STARTSIZE, "9");
        style.put(mxConstants.STYLE_ENDSIZE, "9");
        stylesheet1.putCellStyle("arrow", style);

        style = new Hashtable<String, Object>();
        //style.put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ORTHOGONAL);
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CONNECTOR);
        style.put(mxConstants.STYLE_FONTSIZE, "15");
        style.put(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE);
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_STROKECOLOR, Color.BLACK); // default is #6482B9
        style.put(mxConstants.STYLE_STARTFILL, Color.WHITE);
        style.put(mxConstants.STYLE_ENDFILL, Color.WHITE);
        style.put(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_OVAL);
        style.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_BLOCK);
        style.put(mxConstants.STYLE_EDITABLE, "1");
        style.put(mxConstants.STYLE_STARTARROW, mxConstants.ARROW_OVAL);
        style.put(mxConstants.STYLE_ENDARROW, mxConstants.ARROW_BLOCK);

        style.put(mxConstants.STYLE_STARTSIZE, "9");
        style.put(mxConstants.STYLE_ENDSIZE, "9");

        style.put(mxConstants.STYLE_DASHED, true);
        style.put(mxConstants.STYLE_EDITABLE, "1");
        stylesheet1.putCellStyle("dashArrow", style);
    }

    // Создает стиль для элементов в форме ромба
    private Hashtable<String, Object> createStyleForIconElementWithRHOMNUS(String file) {
        Hashtable<String, Object> style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_IMAGE);
        style.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_RHOMBUS);
        style.put(mxConstants.STYLE_IMAGE, file);
        style.put(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_BOTTOM);
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_FONTSIZE, "12");
        style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        style.put(mxConstants.STYLE_STROKECOLOR, "#000000");
        style.put(mxConstants.STYLE_FILLCOLOR, "#ffffff");
        style.put(mxConstants.STYLE_WHITE_SPACE, "wrap");
        style.put(mxConstants.STYLE_RESIZABLE, "0");
        return style;
    }

    // Создает стиль для элемента в форме эллипса
    private Hashtable<String, Object> createStyleForIconElementWithELLIPSE(String file) {
        Hashtable<String, Object> style = new Hashtable<String, Object>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_IMAGE);
        style.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_ELLIPSE);
        style.put(mxConstants.STYLE_IMAGE, file);
        style.put(mxConstants.STYLE_VERTICAL_LABEL_POSITION, mxConstants.ALIGN_BOTTOM);
        style.put(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER);
        style.put(mxConstants.STYLE_FONTSIZE, "12");
        style.put(mxConstants.STYLE_FONTCOLOR, "#000000");
        style.put(mxConstants.STYLE_STROKECOLOR, "#000000");
        style.put(mxConstants.STYLE_FILLCOLOR, "#ffffff");
        style.put(mxConstants.STYLE_WHITE_SPACE, "wrap");
        style.put(mxConstants.STYLE_RESIZABLE, "0");
        return style;
    }

    // Передвигает все элементы на заданную разницу позиций
    private void moveAllElements(double dx, double dy) {
        getModel().beginUpdate();
        selectAll();
        moveCells(this.getSelectionCells(), dx, dy);
        setSelectionCell(null);
        getModel().endUpdate();
    }
}
