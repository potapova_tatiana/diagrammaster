/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FindPath;

/**
 * Узел для расчета пути
 * @author trungchi321
 */
public class Node implements Comparable {

    //направления
    public static String E = "EAST";    
    public static String W = "WEST";    
    public static String N = "NORTH";
    public static String S = "SOUTH";

    //координаты
    public Point point; 
    public int f;       
    public int g;
    
    //узел-родитель
    public Node parent;
    
    //направление 
    public String direction = "root";

    /**
    *
    * Конструктор
    */
    public Node() {
        point = null;
        parent = null;
        g = f = 0;
    }

    /**
    *
    * Конструктор
    * @param Point координата
    */
    public Node(Point point) {
        this();
        this.point = point;
    }

    /**
    *
    * Конструктор
    * @param x коорданата x
    * @param y коорданата y
    * @param parent узел-родитель
    */
    public Node(int x, int y, Node parent) {
        this();
        point = new Point(x, y);
        this.parent = parent;
        setDirection();
    }

    /**
    *
    * Конструктор
    * @param parent узел-родитель
    */
    public Node(Node node) {
        this();
        parent = node;
        g = node.g;
        f = node.f;
    }

    /**
    *
    * Сравнивает с объектом
    * @param o сравниваемый объект
    * @return результат сравнения
    */
    @Override
    public int compareTo(Object o) {
        Node node = (Node) o;

        int comissi = 0;

        if (this.parent.direction.equalsIgnoreCase(this.direction)) {
            comissi = -10;
        }

        return comissi + (int) (f - node.f);
    }

    /**
    *
    * Устанавливает направление
    */
    private void setDirection() {
        if (parent != null) {
            if (parent.point.x == this.point.x) {
                if (parent.point.y < this.point.y) {
                    this.direction = S;
                } else {
                    this.direction = N;
                }
            } else {
                if (parent.point.x < this.point.x) {
                    this.direction = E;
                } else {
                    this.direction = W;
                }
            }
        }
    }
}
