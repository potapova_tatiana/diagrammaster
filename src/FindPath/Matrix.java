/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FindPath;

import bmpn.GraphView;
import com.mxgraph.model.mxCell;
import com.mxgraph.util.mxPoint;
import java.util.List;

/**
 * Матрица, иммитирующая граф с точки зрения занятости позиций
 * @author trungchi321
 */
public class Matrix {

    private int[][] Matrix;             //матрица
    private final GraphView graphView;  //граф
    public static int SCALE = 4;

    /**
    *
    * Конструктор
    * @param graphView граф
    */
    public Matrix(GraphView graphView) {
        this.graphView = graphView;
    }

    /**
    *
    * Устанавливает размеры матрицы
    * @param width ширина
    * @param height высота
    */
    public void setSize(int width, int height) {

        Matrix = new int[width / SCALE + 1][height / SCALE + 1];

        for (int[] Matrix1 : Matrix) {
            for (int j = 0; j < Matrix1.length; j++) {
                Matrix1[j] = 1;
            }
        }

        addElementsToMatrix();
    }

    /**
    *
    * Возвращает матрицу
    * @return размеры матрицы
    */
    public int[][] getMatrix() {
        return Matrix;
    }

    /**
    *
    * Добавляет элементы в матрицу
    */
    public void addElementsToMatrix() {

        Object[] vertexs = graphView.getChildCells(graphView.getDefaultParent(), true, false);
        Object[] edges = graphView.getChildEdges(graphView.getDefaultParent());

        for (Object vertex : vertexs) {
            addVertexToMatrix((mxCell) vertex);
        }

        for (Object edge : edges) {
            addEdgeToMatrix((mxCell) edge);
        }
    }

    /**
    *
    * Добавляет элемент типа Vertex  в матрицу
    * @param vertex добавляемый элемент
    */
    public void addVertexToMatrix(mxCell vertex) {
        int x = (int) vertex.getGeometry().getX();
        int y = (int) vertex.getGeometry().getY();
        int height = (int) vertex.getGeometry().getHeight();
        int width = (int) vertex.getGeometry().getWidth();

        for (int i = x / SCALE - 1; i <= (x + width) / SCALE + 1; i++) {
            for (int j = y / SCALE - 1; j <= (y + height) / SCALE + 1; j++) {
                Matrix[i][j] = 0;
            }
        }
    }

    /**
    *
    * Удаляет элемент типа Vertex из матрицу
    * @param vertex удаляемый элемент
    */
    public void deleteVertexFromMatrix(mxCell vertex) {
        int x = (int) vertex.getGeometry().getX();
        int y = (int) vertex.getGeometry().getY();
        int height = (int) vertex.getGeometry().getHeight();
        int width = (int) vertex.getGeometry().getWidth();

        for (int i = x / SCALE - 1; i <= (x + width) / SCALE + 1; i++) {
            for (int j = y / SCALE - 1; j <= (y + height) / SCALE + 1; j++) {
                Matrix[i][j] = 1;
            }
        }
    }

    /**
    *
    * Добавляет элемент типа Edge в матрицу
    * @param vertex добавляемый элемент
    */
    public void addEdgeToMatrix(mxCell edge) {
        List<mxPoint> points = graphView.getView().getState(edge).getAbsolutePoints();

        for (int i = 0; i < points.size() - 1; i++) {
            mxPoint point1 = points.get(i);
            mxPoint point2 = points.get(i + 1);

            if (point1.getX() == point2.getX()) {

                int x = (int) (point1.getX() / SCALE);

                if (point1.getY() < point2.getY()) {
                    for (int j = (int) (point1.getY() / SCALE); j <= (int) (point2.getY() / SCALE); j++) {
                        Matrix[x][j] = 0;
                    }
                } else {
                    for (int j = (int) (point2.getY() / SCALE); j <= (int) (point1.getY() / SCALE); j++) {
                        Matrix[x][j] = 0;
                    }
                }
            } else {
                int y = (int) (point1.getY() / SCALE);

                if (point1.getX() < point2.getX()) {
                    for (int j = (int) (point1.getX() / SCALE); j <= (int) (point2.getX() / SCALE); j++) {
                        Matrix[j][y] = 0;
                    }
                } else {
                    for (int j = (int) (point2.getX() / SCALE); j <= (int) (point1.getX() / SCALE); j++) {
                        Matrix[j][y] = 0;
                    }
                }
            }
        }
    }

    /**
    *
    * Удаляет элемент типа Edge из матрицы
    * @param vertex удаляемый элемент
    */
    public void deleteEdgeFromMatrix(mxCell edge) {
        List<mxPoint> points = graphView.getView().getState(edge).getAbsolutePoints();

        for (int i = 0; i < points.size() - 1; i++) {
            mxPoint point1 = points.get(i);
            mxPoint point2 = points.get(i + 1);

            if (point1.getX() == point2.getX()) {

                int x = (int) (point1.getX() / SCALE);

                if (point1.getY() < point2.getY()) {
                    for (int j = (int) (point1.getY() / SCALE); j <= (int) (point2.getY() / SCALE); j++) {
                        Matrix[x][j] = 1;
                    }
                } else {
                    for (int j = (int) (point2.getY() / SCALE); j <= (int) (point1.getY() / SCALE); j++) {
                        Matrix[x][j] = 1;
                    }
                }
            } else {
                int y = (int) (point1.getY() / SCALE);

                if (point1.getX() < point2.getX()) {
                    for (int j = (int) (point1.getX() / SCALE); j <= (int) (point2.getX() / SCALE); j++) {
                        Matrix[j][y] = 1;
                    }
                } else {
                    for (int j = (int) (point2.getX() / SCALE); j <= (int) (point1.getX() / SCALE); j++) {
                        Matrix[j][y] = 1;
                    }
                }
            }
        }
    }

    /**
    *
    * Представляет матрицу в виде строки
    * @return матрица в виде строки
    */
    @Override
    public String toString() {
        String temp = "";

        for (int[] Matrix1 : Matrix) {
            temp += "{";
            for (int j = 0; j < Matrix1.length - 1; j++) {
                temp += Matrix1[j] + ", ";
            }
            temp += Matrix1[Matrix1.length - 1] + "},\n";
        }

        return temp;
    }

}
