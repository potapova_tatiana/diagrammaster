/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FindPath;

/**
 * Точка
 * @author trungchi321
 */
public class Point {

    public int x;   //координата х
    public int y;   //координата у

    /**
    *
    * Конструктор
    * @param x координата х
    * @param y координата y
    */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
    *
    * Представляет в виде строки
    * @return строковое представление
    */
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }
}
