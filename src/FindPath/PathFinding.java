/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FindPath;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Нахождит оптимальный путь
 * @author trungchi321
 */
public class PathFinding {

    public Point start; // начальная точка
    public Point goal;  // конечная точка
    public int[][] map; // карта значений
    
    // вспомогательные списки для расчета
    PriorityQueue<Node> open = new PriorityQueue<>();   
    ArrayList<Node> close = new ArrayList<>();

    /**
    *
    * Конструктор
    * @param map карта
    * @param goal цель
    */
    public PathFinding(int[][] map, Point goal) {
        this.map = map;
        this.goal = goal;
    }

    /**
    *
    * Строит путь
    * @param from откуда
    * @param to куда
    * @return возможность прохода
    */
    public int g(Node from, Node to) {

        if (from.point.x == to.point.x && from.point.y == to.point.y) {
            return 0;
        }

        if (map[to.point.y][to.point.x] == 1) {

//            if (from.point.y == to.point.y) {
//                return 10;
//            }
            return 10;
        }

        return Integer.MAX_VALUE;
    }

    /**
    *
    * Определяет, достигнута ли цель
    * @param node текущий узел
    * @return достигнута ли цель
    */
    public boolean isGoal(Node node) {
        return (node.point.x == goal.x) && (node.point.y == goal.y);
    }

    /**
    *
    * Рассчитывает высоту прохода
    * @param current текущий узел
    * @return высота прохода
    */
    public int h(Node current) {
        return 10 * Math.abs(goal.x - current.point.x) + 10 * Math.abs(goal.y - current.point.y);
    }

    /**
    *
    * Генерирует вспомогательный список для рассчетов
    * @param node текущий узел
    * @return вспомогательный список узлов
    */
    protected List<Node> generateSuccessors(Node node) {

        List<Node> ret = new LinkedList<>();
        int x = node.point.x;
        int y = node.point.y;
        int width = map[0].length - 1;

        if (y < map.length - 1 && map[y + 1][x] == 1) {
            ret.add(new Node(x, y + 1, node));
        }

        if (y > 0 && map[y - 1][x] == 1) {
            ret.add(new Node(x, y - 1, node));
        }

        if (x < width && map[y][x + 1] == 1) {
            ret.add(new Node(x + 1, y, node));
        }

        if (x > 0 && map[y][x - 1] == 1) {
            ret.add(new Node(x - 1, y, node));
        }

        return ret;
    }

    /**
    *
    * Рассчитывает длину прохода
    * @param node текущий узел
    * @return длина прохода
    */
    protected int f(Node node) {

        int g = 0;

        if (node.parent != null) {
            g = g(node.parent, node) + ((node.parent != null) ? node.parent.g : 0);
        }

        int h = h(node);

        node.g = g;
        node.f = g + h;

        return node.f;
    }

    /**
    *
    * Находит нальнейший путь
    * @param node текущий узел
    */
    public void expand(Node node) {

        if (!checked(node)) {
            close.add(node);
        }

        List<Node> successors = generateSuccessors(node);

        for (Node t : successors) {
            f(t);

            if (!checked(t) && !checkopen(t)) {
                open.offer(t);
            }
        }
    }

    /**
    *
    * Рассчитывает длину прохода
    * @param node текущий узел
    * @return длина прохода
    */
    public boolean checked(Node node) {
        for (Node n : close) {
            if (n.point.x == node.point.x && n.point.y == node.point.y) {
                return true;
            }
        }

        return false;
    }

    /**
    *
    * Проверяет, открыт ли узел
    * @param node текущий узел
    * @return флаг открыт ли путь
    */
    public boolean checkopen(Node node) {
        for (Node n : open) {
            if (n.point.x == node.point.x && n.point.y == node.point.y) {
                return true;
            }
        }

        return false;
    }

    /**
    *
    * Рассчитывает точки для построения пути
    * @param start начальная точка
    * @return список точек
    */
    public List<Point> compute(Point start) {
        try {
            Node node = new Node(start);

            /* Needed if the initial point has a cost.  */
            f(node);

            expand(node);

            for (;;) {
                Node p = open.poll();

                if (p != null && p.point != null) {
                    System.out.println(p.point);
                }

                if (p == null) {
                    return null;
                }

                if (isGoal(p)) {
                    LinkedList<Point> retPath = new LinkedList<Point>();

                    for (Node i = p; i != null; i = i.parent) {
                        retPath.addFirst(i.point);
                    }

                    System.out.print("Path = ");
                    for (Point n : retPath) {
                        System.out.print(n);
                    }
                    System.out.println();

                    return retPath;
                }

                expand(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("No path");
        return null;

    }

}
